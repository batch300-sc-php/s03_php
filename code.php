<?php


//<!-- Activity s03 -->
class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is {$this->firstName} {$this->middleName} {$this->lastName}";
    }

}

$person = new Person('Senku', '', 'Ishigami');

class Developer extends Person {
    public function printName(){
        return "Your full name is {$this->firstName} {$this->middleName} {$this->lastName} and you are a developer";
    }
}

class Engineer extends Person {
    public function printName(){
        return "You are an engineer named {$this->firstName} {$this->middleName} {$this->lastName}";
    }
}

// $person = Person('Senku', ' ', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');


//=====================<!--Supplementary Activity  -->=========================//

class Character {
    public $name;

    public function __construct($name) {
        $this->name = $name;
    }

    function printName(){

    }
}

class VoltesMember extends Character{
    public $vehicle;
    
    public function __construct($name, $vehicle) {
        parent::__construct($name);
        $this->vehicle = $vehicle;
    }

    function printName(){
        return "Hi, I am {$this->name}! I pilot the {$this->vehicle}";
    }
}

$steve = new VoltesMember('Steve', "Volt Cruiser");
$bigBert = new VoltesMember('Big Bert', "Volt Panzer");
$littleJohn = new VoltesMember('Little John', "Volt Frigate");
$jamie = new VoltesMember('Jamie', "Volt Lander");
$mark = new VoltesMember('Mark', "Volt Bomber");
