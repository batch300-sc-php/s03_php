<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S03: Classes and Objects</title>
    </head>
    <body>
     
    <!--=====================Activity s03 =================================== -->
    <h1>Person</h1>
    <p><?php echo $person->printName() . "."; ?></p>

    <h1>Developer</h1>
    <p><?php echo $developer->printName() . "."; ?></p>

    <h1>Engineer</h1>
    <p><?php echo $engineer->printName() . "."; ?></p>

    <hr>


    <!--=====================Supplementary Activity ========================= -->
    <p><?= var_dump($steve); ?></p>
    <p><?= var_dump($bigBert); ?></p>
    <p><?= var_dump($littleJohn); ?></p>
    <p><?= var_dump($jamie); ?></p>
    <p><?= var_dump($mark); ?></p>
    
<hr>

    <p><?php echo $steve->printName() . "."; ?></p>
    <p><?php echo $bigBert->printName() . "."; ?></p>
    <p><?php echo $littleJohn->printName() . "."; ?></p>
    <p><?php echo $jamie->printName() . "."; ?></p>
    <p><?php echo $mark->printName() . "."; ?></p>

    </body>
</html>